using System.Collections.Generic;

namespace DictAPI.Models{

    public class UserProfileReadDto{
        public int UsersId { get; set; }
        public string Username { get; set; }
        public string Pwd { get; set; }
        public string Email { get; set; }

       // public virtual ICollection<Floders> Floders { get; set; }
    }
}