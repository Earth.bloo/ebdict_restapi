namespace DictAPI.Dtos{

    public class VocabReadDto{

        
        public int VocabId { get; set; }
        public string VocabName { get; set; }
        public string Pronunciation { get; set; }
        public string TypeWord { get; set; }
        public string Defination { get; set; }
        public string Example { get; set; }

        public string synonym { get; set; }




    }

}