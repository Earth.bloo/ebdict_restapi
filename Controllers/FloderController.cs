using Microsoft.AspNetCore.Mvc;
using DictAPI.Models;
using System.Collections.Generic;
using DictAPI.Data;
using AutoMapper;
using DictAPI.Dtos;
using System.Threading.Tasks;
using System;
using System.Data.Entity;

namespace DictAPI.Controllers
{
    [Route("api/floder")]
    [ApiController]
        public class FloderControllers: ControllerBase
        {
        private readonly IDictRepo<Floders> _repository;
        private readonly IMapper _mapper;

         public FloderControllers(IDictRepo<Floders> repository)
            {
                _repository = repository;
            }
        
        [HttpGet]
           // public IEnumerable<Vocaburary> GetAllVocab() => _repository.GetAll();
           public async Task<ActionResult<IEnumerable<FloderReadDto>>> GetAllFloder(){
                var floderitem = await _repository.GetAll(b => b.Include(b => b.Users));



                if (floderitem == null){
                    return NotFound();
                }
                

                List<FloderReadDto> response = new List<FloderReadDto>();
                floderitem.ForEach(floderitem => 
                {
                    
                    response.Add(new FloderReadDto 
                    {
                        FloderId = floderitem.Id,
                        FloderName = floderitem.FloderName,
                        UsersId = floderitem.UsersId,
                        Users = floderitem.Users
                    });
                });
                return response;
            }




            [HttpGet("{id}")]
            public async Task<ActionResult<VocabReadDto>>  GetVocabById(int id){
                var floderitem = await _repository.GetByID(id);
                if (floderitem == null){
                    return NotFound();
                }
                VocabReadDto response = new VocabReadDto();
                // response.VocabId = floderitem.Id;
                // response.Pronunciation = floderitem.Pronunciation;
                // response.VocabName = floderitem.VocabName;
            
                return response;
            }

        //    [HttpGet("{id}")]
        //     public async Task<ActionResult<VocabReadDto>>  GetVocabById(int id){
        //         var vocabitem = await _repository.GetByID(id);
        //         if (vocabitem == null){
        //             return NotFound();
        //         }
        //         VocabReadDto response = new VocabReadDto();
                
        //         return response;
        //     }


        }
}
