using Microsoft.AspNetCore.Mvc;
using DictAPI.Models;
using System.Collections.Generic;
using DictAPI.Data;
using AutoMapper;
using DictAPI.Dtos;
using System.Threading.Tasks;
using System;
using System.Data.Entity;

namespace DictAPI.Controllers{
    [Route("api/user")]
    [ApiController]
        public class UserControllers: ControllerBase{
        private readonly IDictRepo<Users> _repository;
        private readonly IMapper _mapper;

         public UserControllers(IDictRepo<Users> repository,IMapper mapper)
            {
                _repository = repository;
                //_mapper = mapper;
            }

        [HttpGet]

       
         public async Task<ActionResult<IEnumerable<UserProfileReadDto>>> GetAllUsers(){
                var useritem = await _repository.GetAll(d => d.Include(d => d.Floders));
                if(useritem == null){
                    return NotFound();
                }


                List<UserProfileReadDto> response = new List<UserProfileReadDto>();
                useritem.ForEach(user => {

                    // List<FloderReadDto> floder_response = new List<FloderReadDto>();
                    // user.Floders.ForEach(x => {
                    //    floder_response.Add(new FloderReadDto{
                    //        FloderId = x.Id,
                    //        FloderName = x.FloderName,
  
                    //    });

                    // });
                   


                    response.Add(new UserProfileReadDto{
                        UsersId = user.Id,
                       
                        Username = user.Username,
                        Pwd = user.Pwd,
                        Email = user.Email
                    });

                });

                return response;

           }

           [HttpGet("{id}")]
            public async Task<ActionResult<Users>>  GetUserById(int id){
                var vocabitem = await _repository.GetByID(id);
                if (vocabitem != null){
                    return Ok(vocabitem);
                }
                return NotFound();

            }



        }

        }
