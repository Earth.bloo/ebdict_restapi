using Microsoft.AspNetCore.Mvc;
using DictAPI.Models;
using System.Collections.Generic;
using DictAPI.Data;
using AutoMapper;
using DictAPI.Dtos;
using System.Threading.Tasks;
using System;
using System.Data.Entity;

namespace DictAPI.Controllers{
    [Route("api/vocab")]
    [ApiController]
        public class VocabControllers: ControllerBase{
        private readonly IDictRepo<Vocaburary> _repository;
     //   private readonly IMapper _mapper;

        public VocabControllers(IDictRepo<Vocaburary> repository,IMapper mapper)
            {
                _repository = repository;
                //_mapper = mapper;
            }

            
            [HttpGet]

           // public IEnumerable<Vocaburary> GetAllVocab() => _repository.GetAll();
            public async Task<ActionResult<IEnumerable<VocabReadDto>>> GetAllVocab(){
                var vocabitem = await _repository.GetAll();
                if (vocabitem == null){
                    return NotFound();
                }
                List<VocabReadDto> response = new List<VocabReadDto>();
                vocabitem.ForEach(vocabitem => 
                {
                    response.Add(new VocabReadDto 
                    {
                        VocabId = vocabitem.Id,
                        Pronunciation = vocabitem.Pronunciation,
                        VocabName = vocabitem.VocabName,
                        TypeWord = vocabitem.TypeWord,
                        Defination = vocabitem.Defination,
                        synonym = vocabitem.synonym,
                        Example = vocabitem.Example
                    });
                });
                return response;
            }

           [HttpGet("{id}")]
            public async Task<ActionResult<VocabReadDto>>  GetVocabById(int id){
                var vocabitem = await _repository.GetByID(id);
                if (vocabitem == null){
                    return NotFound();
                }
                VocabReadDto response = new VocabReadDto();
                response.VocabId = vocabitem.Id;
                response.Pronunciation = vocabitem.Pronunciation;
                response.VocabName = vocabitem.VocabName;
                response.TypeWord = vocabitem.TypeWord;
                response.Defination = vocabitem.Defination;
                response.synonym = vocabitem.synonym;
                response.Example = vocabitem.Example;
                return response;
            }
            // [HttpGet("floder")]

            //  public ActionResult<IEnumerable<FloderReadDto>> GetAllFloder(){
            //     var vocabitem = _repository.GetAllFloder();
            //     return Ok(_mapper.Map<IEnumerable<FloderReadDto>>(vocabitem));
            //  }

            //  [HttpGet("floder/{id}")]
            // public async Task<ActionResult<FloderReadDto>> GetFloderByUserId(int id){
            //     var vocabitem = await _repository.GetFloderByUserID(id);
            //     if (vocabitem != null){
            //         return Ok(_mapper.Map<IEnumerable<FloderReadDto>>(vocabitem));
            //     }
            //     return NotFound();

            // }

            // [HttpGet("user/{id}")]
            // public async Task<ActionResult<UserProfileReadDto>> GetUserProfileByUserId(int id){
            //     var vocabitem = await _repository.GetUserProfileByID(id);
            //     if (vocabitem != null){
            //         return Ok(_mapper.Map<UserProfileReadDto>(vocabitem));
            //     }
            //     return NotFound();

            // }

            // [HttpGet("floder/detail/{id}")]

            //  public async Task<ActionResult<IEnumerable<FloderDetailReadDto>>> GetVocabInFloderByFloderID(int id){
            //     var vocabitem = await _repository.GetFloderDetailByFloderID(id);
            //     if(vocabitem != null){
            //     return Ok(_mapper.Map<IEnumerable<FloderDetailReadDto>>(vocabitem));
            //  }
            //  return NotFound();
            //  }



             
        
    }
}  
