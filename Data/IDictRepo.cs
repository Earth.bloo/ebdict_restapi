using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DictAPI.Models;

namespace DictAPI.Data{
    public interface IDictRepo<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAll();
        Task<IEnumerable<T>> GetAll(Func<IQueryable<T>, IQueryable<T>> func);
        Task<T> GetByID(int id);

        void Insert(T entity);

        void Delete(Guid id);

        

        // IEnumerable<Floders>  GetAllFloder();

        //Task<IEnumerable<Floders>> GetFloderByUserID(int id);


    

        // Task<Users> GetUserProfileByID(int id);



        // Task<IEnumerable<Vocabinfloder>> GetFloderDetailByFloderID(int id);

    }
}