using System;

namespace DictAPI.Data{
    public class BaseEntity : IEntity
    {
        public Guid Id { get; set;}
        
    }
}