using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using DictAPI.Models;
using System;

namespace DictAPI.Data{
    public class SqlDictRepo<T> : IDictRepo<T> where T: BaseEntity
    {
        private readonly DictContext _context;
        private DbSet<T> entities;
        public SqlDictRepo(DictContext context)
        {
            _context = context;
            entities = _context.Set<T>();
            
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await entities.ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAll(Func<IQueryable<T>, IQueryable<T>> func)
        {
            IQueryable<T> resultWithEagerLoading = func(entities);
            return await resultWithEagerLoading.ToListAsync();
        }


        public async Task<T> GetByID(int id)
        {
            return await _context.Set<T>().FindAsync();
        }

       
        

     

        // public IEnumerable<Vocaburary> GetAllVocab()
        // {
        //     return _context.Vocaburary.ToList();
        // }

        // public async  Task<IEnumerable<Floders>> GetFloderByUserID(int id)
        // {
        //     return await _context.Floders
        //     .Where(f => f.UsersId ==id)
        //     .ToListAsync();
        // }

        // public async Task<Users> GetUserProfileByID(int id)
        // {
        //     return await _context.Users.Include(b => b.Floders)
        //                     .FirstOrDefaultAsync(p => p.Id == id);
            
        //  }

        // public async Task<Vocaburary> GetVocabByID(int id)
        // {
        //     return await _context.Vocaburary.FirstOrDefaultAsync(x => x.VocabId == id);

            

        //     // return _context.Vocaburary.FirstOrDefault(p => p.VocabId == id);
        // }

        // public async Task<IEnumerable<Vocabinfloder>> GetFloderDetailByFloderID(int id)
        // {
        //     return await _context.Vocabinfloder
        //     .Where(f => f.FloderId ==id)
        //     //.Include(b => b.Vocab)
        //     .Include(b => b.Vocab.VocabName)
        //     .ToListAsync();
        // }

        

        public void Insert(T entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        
    }
}