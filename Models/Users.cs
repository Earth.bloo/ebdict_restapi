﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DictAPI.Data;
using Newtonsoft.Json;

namespace DictAPI.Models
{
    public partial class Users:BaseEntity
    {
        public Users()
        {
            Floders = new HashSet<Floders>();
        }
        [Column("UserId")]
        public int Id { get; set; }
        public string Username { get; set; }
        public string Pwd { get; set; }
        public string Email { get; set; }
        
        public virtual ICollection<Floders> Floders { get; set; }
    }
}
