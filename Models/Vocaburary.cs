﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DictAPI.Data;
using Microsoft.EntityFrameworkCore;

namespace DictAPI.Models
{
    public partial class Vocaburary:BaseEntity
    {
        public Vocaburary()
        {
            Vocabinfloder = new HashSet<Vocabinfloder>();
        }
        [Column("VocabId")]
        public int Id { get; set; }
        public string VocabName { get; set; }
        public string Pronunciation { get; set; }
        public string TypeWord { get; set; }
        public string Defination { get; set; }
        public string Example { get; set; }
        public string synonym { get; set; }

        public virtual ICollection<Vocabinfloder> Vocabinfloder { get; set; }
    }
}
