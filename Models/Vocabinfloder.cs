﻿using System;
using System.Collections.Generic;
using DictAPI.Data;

namespace DictAPI.Models
{
    public partial class Vocabinfloder
    {
        public int VocabinfloderId { get; set; }
        public int FloderId { get; set; }
        public int VocabId { get; set; }
        public virtual Floders Floder { get; set; }
        public virtual Vocaburary Vocab { get; set; }
    }
}
