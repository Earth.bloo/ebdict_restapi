﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DictAPI.Data;

namespace DictAPI.Models
{
    public partial class Floders:BaseEntity
    {
        public Floders()
        {
            Vocabinfloder = new HashSet<Vocabinfloder>();
        }
        
        [Column("FloderId")]
        public int Id { get; set; }
        public string FloderName { get; set; }
        public int UsersId { get; set; }

        public virtual Users Users { get; set; }
        public virtual IEnumerable<Vocabinfloder> Vocabinfloder { get; set; }
    }
}
