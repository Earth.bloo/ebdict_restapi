﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DictAPI.Models
{
    public partial class  DictContext : DbContext
    {
        public  DictContext()
        {
        }

        public  DictContext( DbContextOptions<DictContext> options) : base(options)
        {
        }

        public virtual DbSet<Floders> Floders { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<Vocabinfloder> Vocabinfloder { get; set; }
        public virtual DbSet<Vocaburary> Vocaburary { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseSqlServer("Server=localhost;Database=DictDB;Trusted_Connection=False;User ID=sa;Password=Earth29chok");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Floders>(entity =>
            {
                entity.HasKey(x => x.Id)
                    .HasName("PK__floders__BFB829EE90AFF837");

                entity.ToTable("floders");

                entity.Property(e => e.Id).HasColumnName("floder_ID");

                entity.Property(e => e.FloderName)
                    .IsRequired()
                    .HasColumnName("floder_Name")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.UsersId).HasColumnName("users_ID");

                entity.HasOne(d => d.Users)
                    .WithMany(p => p.Floders)
                    .HasForeignKey(x => x.UsersId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("userID_to_usersID_in_users_FK");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.ToTable("users");

                entity.Property(e => e.Id).HasColumnName("users_ID");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pwd)
                    .IsRequired()
                    .HasColumnName("pwd")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Vocabinfloder>(entity =>
            {
                entity.ToTable("vocabinfloder");

                entity.Property(e => e.VocabinfloderId).HasColumnName("vocabinfloder_ID");

                entity.Property(e => e.FloderId).HasColumnName("floder_ID");

                entity.Property(e => e.VocabId).HasColumnName("vocab_ID");

                entity.HasOne(d => d.Floder)
                    .WithMany(p => p.Vocabinfloder)
                    .HasForeignKey(x => x.FloderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("vocabinfloder_to_floders_in_floder_ID_FK");

                entity.HasOne(d => d.Vocab)
                    .WithMany(p => p.Vocabinfloder)
                    .HasForeignKey(x => x.VocabId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("vocabid_to_vocab_id_in_vocaburary_FK");
            });

            modelBuilder.Entity<Vocaburary>(entity =>
            {
                entity.HasKey(x => x.Id)
                    .HasName("PK__vocabura__480869DD0FFEE208");

                entity.ToTable("vocaburary");

                entity.Property(e => e.Id).HasColumnName("vocab_ID");

                entity.Property(e => e.Defination)
                    .IsRequired()
                    .HasColumnName("defination")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Example)
                    .HasColumnName("example")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Pronunciation)
                    .IsRequired()
                    .HasColumnName("pronunciation")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.TypeWord)
                    .IsRequired()
                    .HasColumnName("type_Word")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.VocabName)
                    .IsRequired()
                    .HasColumnName("vocab_Name")
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.