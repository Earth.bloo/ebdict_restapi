
using AutoMapper;
using DictAPI.Dtos;
using DictAPI.Models;

namespace DictAPI.Profiles{
    public class VocabProfile : Profile {
        public VocabProfile()
        {
            CreateMap<Vocaburary,VocabReadDto>();
            CreateMap<Users,UserProfileReadDto>();
            CreateMap<Floders,FloderReadDto>();
        }

    }
}